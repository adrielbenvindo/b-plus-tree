const int ORDER = 4

struct BPlusTreeNode{
    BPlusTree* root;
    int count_keys;
    int keys[ORDER-1];
    BPlusTreeNode* nodes[ORDER];
    Leaf* leafs[ORDER];
};

struct Leaf{
    BPlusTree* root;
    int count_keys;
    int keys[ORDER-1];
    Leaf* next;
};

BPlusTreeNode* balance_tree(BPlusTreeNode* root, BPlusTreeNode* tree){
    if(tree != NULL){
        
    }
    return tree;
}

BPlusTreeNode* insert_tree(BPlusTreeNode* root, BPlusTreeNode* tree, int value){
  if(tree == NULL){
    tree = (BPlusTreeNode*) malloc(sizeof(BPlusTreeNode));
    tree->count_keys = 1;
    tree->keys[0] = value;
    tree->root = root;
    tree->leafs[1] = (Leaf*) malloc(sizeof(Leaf))
    tree->leafs[1]->count_keys = 1;
    tree->leafs[1]->keys[0] = value;
    tree->leafs[1]->root = tree;
    tree->leafs[1]->next = NULL;
  }else{
    if(tree->count_keys == 1 && tree->root == NULL){
        if(value < tree->keys[0]){
            tree->leafs[1]->count_keys+=1;
            tree->leafs[1]->keys[1] = tree->keys[0];
            tree->leafs[1]->keys[0] = value;
            tree->keys[0] = value;
        }else{
            if(tree->leafs[1]->count_keys < ORDER - 1){
                int forward = 0;
                for(int numd = 0; numd < tree->leafs[1]->count_keys+1; numd++){
                    if(value >= tree->leafs[1]->keys[numd]){
                        forward = tree->leafs[1]->keys[numd];
                        tree->leafs[1]->keys[numd] = value;
                        value = foward;
                    }
                }
                tree->leafs[1]->count_keys+=1;
            }else{
                int pivot = tree->leafs[1]->count_keys%2;
                tree->keys[1] = tree->leafs[1]->keys[pivot];
                int count_keys = 0;
                for(int numd = pivot; numd < tree->leafs[1]->count_keys; numd++){
                    tree->leafs[2]->keys[count_keys] = tree->leafs[1]->keys[numd];
                }
                tree->leafs[2]->count_keys = count_keys;
                tree->leafs[1]->count_keys = pivot;
            }
        }
    }else{
        for(int numd = 0; numd < tree->count_keys; numd++){
            if(value >= tree->keys[numd]){
                if(tree->nodes[numd+1] != NULL){
                    tree->nodes[numd+1] = insert_tree(tree,tree->nodes[numd+1],value);
                    break;
                }else{
                    if(tree->leafs[numd+1]->count_keys < ORDER - 1){
                        int forward = 0;
                        for(int numd_leaf = 0; numd_leaf < tree->leafs[numd]->count_keys+1; numd_leaf++){
                            if(value >= tree->leafs[numd]->keys[numd_leaf]){
                                forward = tree->leafs[numd]->keys[numd_leaf];
                                tree->leafs[numd]->keys[numd_leaf] = value;
                                value = forward;
                            }
                        }
                        tree->leafs[numd+1]->count_keys+=1;
                    }else{
                        int pivot = tree->leafs[numd+1]->count_keys%2;
                        int count_keys = 0;
                        tree = balance_tree(root,tree);
                    } 
                    break;
                }
            }
            if(value < tree->keys[numd]){
                if(tree->nodes[numd] != NULL){
                    tree->nodes[numd] = insert_tree(tree,tree->nodes[numd],value);
                    break;
                }else{
                    tree->keys[numd] = value;
                    if(tree->leafs[numd]->count_keys < ORDER - 1){
                        int forward = 0;
                        for(int numd_leaf = 0; numd_leaf < tree->leafs[numd]->count_keys+1; numd_leaf++){
                            forward = tree->leafs[numd]->keys[numd_leaf];
                            tree->leafs[numd]->keys[numd_leaf] = value;
                            value = forward;
                        }
                        tree->leafs[numd]->count_keys+=1;
                    }else{
                        int pivot = tree->leafs[numd]->count_keys%2;
                        int count_keys = 0;
                        tree = balance_tree(root,tree);
                    }
                    break;
                }
            }
        }
    }
  }
  return tree;
}

BPlusTreeNode* search_tree(BPlusTreeNode* tree, int value){
    if(tree != NULL){
        int last_key = tree->count_keys - 1;
        if(tree->keys[last_key] < value){
            if(tree->leafs[last_key+1] != NULL){
                for(int numd_leaf = 0; numd_leaf < tree->leafs[last_key+1]->count_keys; numd_leaf++){
                    if(tree->leafs[last_key+1]->keys[numd_leaf] == value){
                        return tree;
                    }
                }
            }
            return search_tree(tree->nodes[last_key+1],value);
        }else{
            for(int numd = 0; numd < last_key; numd++){
                if(tree->keys[numd] == value){
                    return tree;
                }else{
                    if(tree->keys[numd] > value){
                        if(tree->leafs[numd] != NULL){
                            for(int numd_leaf = 0; numd_leaf < tree->leafs[numd]->count_keys; numd_leaf++){
                                if(tree->leafs[numd]->keys[numd_leaf] == value){
                                    return tree;
                                }
                            }
                        }
                        return search_tree(tree->nodes[numd]->value);
                    }
                }
            }
        }
    }
    return NULL;
}

BPlusTreeNode* remove_tree(BPlusTreeNode* root, BPlusTreeNode* tree, int value){
    if(tree != NULL){
        BPlusTreeNode* tree_target = search_tree(tree,value);
        if(tree_target != NULL){
            int backward = 0;
            int last_key_target = tree_target->count_keys -1;
            if(tree_target->keys[last_key] < value){
                for(int numd_leaf = 0; numd_leaf < tree_target->leafs[last_key+1]->count_keys - 1; numd_leaf++){
                    if(tree_target->leafs[last_key+1]->keys[numd_leaf] >= value && tree_target->leafs[last_key+1]->keys[numd_leaf+1] > value){
                        backward = tree_target->leafs[last_key+1]->keys[numd_leaf];
                        tree_target->leafs[last_key+1]->keys[numd_leaf] = tree_target->leafs[last_key+1]->keys[numd_leaf+1];
                    }
                }
            }else{
                for(int numd = 0; numd < last_key_target; numd++){
                    if(tree_target->keys[numd] > value){
                        for(int numd_leaf = 0; numd_leaf < tree_target->leafs[numd]->last_key - 1; numd_leaf++){
                            if(tree_target->leafs[numd]->keys[numd_leaf] >= value && tree_target->leafs[numd]->keys[numd_leaf+1] > value){
                                backward = tree_target->leafs[numd]->keys[numd_leaf];
                                tree_target->leafs[numd]->keys[numd_leaf] = tree_target->leafs[numd]->keys[numd_leaf+1];
                            }
                        }
                    }else{
                        if(tree_target->keys[numd] == value){
                            for(int numd_leaf = 0; numd_leaf < tree_target->leafs[numd+1]->count_keys - 1; numd_leaf++){
                                if(tree_target->leafs[numd+1]->keys[numd_leaf] >= value && tree_target->leafs[numd+1]->keys[numd_leaf+1] > value){
                                    backward = tree_target->leafs[numd+1]->keys[numd_leaf];
                                    tree_target->leafs[numd+1]->keys[numd_leaf] = tree_target->leafs[numd+1]->keys[numd_leaf+1];
                                }
                            }
                        }
                    }
                }
            }
            tree_target = balance_tree(tree_target->root,tree_target);
        }
    }
    return tree;
}


