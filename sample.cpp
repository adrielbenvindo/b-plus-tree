#include <iostream>
#include "ordenation.hpp"
#include "b_plus_tree.hpp"

using namespace std;

void show_tree(BPlusTree* tree);

int main(){
  menu(NULL,NULL);
}

void menu(BPlusTreeNode* root, BPlusTreeNode* tree){
  int option;
  cout<<"Options:"<<endl;
  cout<<"0 - Exit"<<endl;
  cout<<"1 - Insert"<<endl;
  cout<<"2 - Remove"<<endl;
  cout<<"3 - Search"<<endl;
  cout<<"4 - Show"<<endl;
  cout<<"choose: ";
  cin>>option;
  system("cls");
  int value;
  if(option == 1 || option == 2 || option == 3){
    cout<<"input the value: ";
    cin>>value;
    system("cls");
  }
  switch(option){
    case 0:
      exit(0);
      break;
    case 1:
    {
        tree = insert_tree(root,tree,value);
        root = tree->root;
    }
      break;
    case 2:
    {
        tree = remove_tree(root,tree,value);
        if (tree!= NULL)
            root = tree->root;
        else
            root = NULL;
    }
      break;
    case 3:
    {
        if(search_tree(tree,value) != NULL)
            cout<<"found";
        else
            cout<<"not found";
    }
      break;
    case 4:
      show_tree(tree,0);
      break;
    default:
      cout<<"invalid option"<<endl;
      system("pause");
      system("cls");
  }
  menu(root,tree);
}

void show_tree(BPlusTreeNode* tree,line){
  if(tree != NULL){
    cout<<" "<<line;
    for(int numd = 0; numd < tree->count_keys; numd++){
        cout<<" "<<tree->keys[numd]<<" ";
    }
    cout<<endl;
    if(tree->nodes[0] != NULL){
        for(int numd = 0; numd < tree->count_keys; numd++){
            for(int numd_node = 0; numd_nodes < tree->node[numd]; numd_node++){
                show_tree(tree->nodes[numd_node],line+1);
            }
        }
    }else{
        line+=1;
        for(int numd = 0; numd < tree->count_keys; numd++){
            if(tree->leafs[numd] != NULL){
                for(int numd_leaf = 0; numd_leaf < tree->leaf[numd]->count_keys; numd_leaf++){
                    cout<<"  "<<tree->leafs[numd]->keys[numd_leaf]<<"  ";
                }
            }
            cout<<" "<<line<<" up";
        }
    }
    cout<<endl;
  }
}
